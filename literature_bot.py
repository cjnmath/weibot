import requests
import re
import json
import urllib
import base64
import rsa
import binascii
from random import choice
from random import randint
from lxml.html import fromstring
import time
import datetime
import glob
import os


DESKTOP_USER_AGENTS = [
                        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36 OPR/48.0.2685.52',
                        'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.9 Safari/537.36',
                        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063'
                        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/65.0.3325.181 Chrome/65.0.3325.181 Safari/537.36',
                        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',
                        'Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/31.0',
                        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.75 Safari/537.36',
                        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.78 Safari/537.36 OPR/47.0.2631.55',
                        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:56.0) Gecko/20100101 Firefox/56.0',
                        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/604.3.5 (KHTML, like Gecko) Version/11.0.1 Safari/604.3.5',
                        ]


class weibo_desktop:
    # for weibo destop webpage login
    def __init__(self, username, password):
        self.username = username
        self.password = password
        url = 'https://login.sina.com.cn/sso/login.php?client=ssologin.js(v1.4.19)'
        headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7',
            'Cache-Control': 'max-age=0',
            'Connection': 'keep-alive',
            'Content-Length': '839',
            'Content-Type': 'application/x-www-form-urlencoded',
            'DNT': '1',
            'Host': 'login.sina.com.cn',
            'Origin': 'https://weibo.com',
            'Referer': 'https://weibo.com/',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': choice(DESKTOP_USER_AGENTS)}
        # the login contains three redirection
        session = requests.Session()
        prelogin_data = self.get_prelogin_args()
        #
        response = session.post(url,
                                headers=headers,
                                data=self.build_post_data(prelogin_data))
        response.encoding = 'GBK'
        url_pattern = re.compile('location\.replace\(\"(.*)\"\)')
        login_url = url_pattern.search(response.text).group(1)
        redirect_response = session.get(login_url)
        redirect_response.encoding = 'GBK'
        redirect_url_pattern = re.compile('location\.replace\(\'(.*)\'\)')
        redirect_url = redirect_url_pattern.search(redirect_response.text).group(1)
        real_response = session.get(redirect_url)
        json_pattern = re.compile('feedBackUrlCallBack\((.*)\)')
        data = json.loads(json_pattern.search(real_response.text).group(1))
        userdomain = data['userinfo']['userdomain']
        uniqueid = data['userinfo']['uniqueid']
        self.user_id = uniqueid
        final_url = 'https://weibo.com/u/{}/home{}'.format(uniqueid, userdomain)
        final_response = session.get(final_url)
        self.home_url = final_response.url
        final_response.encoding = 'UTF-8'
        self.session = session
        # this following two line isn't neccessary
        # I'm fancy of doing scrapying at first
        root = fromstring(final_response.text)
        self.home_root = root

    def get_prelogin_args(self):
        """ the prelog data contains the rsa public key and other neccessary information to login
            it is somthing like this :
            {
            'entry': 'weibo',
            'gateway': '1',
            'from': '',
            'savestate': '7',
            'qrcode_flag': 'false',
            'useticket': '1',
            'pagerefer': 'https://passport.weibo.com/visitor/visitor?entry=miniblog&a=enter&url=https%3A%2F%2Fweibo.com%2F&domain=.weibo.com&ua=php-sso_sdk_client-0.6.28&_rand=1524785463.8471',
            'vsnf': '1',
            'su': 'Y2pubWF0aCU0MDEyNi5jb20=',
            'service': 'miniblog',
            'servertime': '1524785533',
            'nonce': 'NE1OVR',
            'pwencode': 'rsa2',
            'rsakv': '1330428213',
            'sp': '60f6dd8c3d28794277e218e5829887c7a97167c842981f4bead3864c6dd42b897ba7a7a2540314e873e603ba688dbfdf470fb404fd571cd5497e23075231a8f55b42',
            'sr': '1920*1080',
            'encoding': 'UTF-8',
            'prelt': '31',
            'url': 'https://weibo.com/ajaxlogin.php?framelogin=1&callback=parent.sinaSSOController.feedBackUrlCallBack',
            'returntype': 'META'
            }

            'su' represents the encrypted username
            'sp' reppresents the rsa encrypted password
        """
        # " su=******** " the ******** is a token encoding (base64) from weibo username
        url = 'https://login.sina.com.cn/sso/prelogin.php?entry=weibo&callback=sinaSSOController.preloginCallBack&su={}&rsakt=mod&checkpin=1&client=ssologin.js(v1.4.19)'
        json_pattern = re.compile('\((.*)\)')
        response = requests.post(url.format(self.get_encrypted_name()))
        data = json.loads(json_pattern.search(response.text).group(1))
        return data

    def get_encrypted_name(self):
        # first, change user name to url encoding,
        # e.g. "@" would be encoding in "%40"
        # or ursing somthing like this
        # username_urllike = urllib.request.path2ulr(self.username)
        username_urllike = urllib.request.quote(self.username)
        # then encoding in base64
        # but has to encode in byte like before pass in
        # because b64encode only accept byte
        username_encrypted = base64.b64encode(username_urllike.encode('utf-8'))
        return username_encrypted.decode('utf-8')

    def get_encrypted_password(self, data):
        """ the password was transmited after (rsa) encrypted
        """
        rsa_e = 65537  # 0x10001 in hexadecimal
        # the password was not encrypted directively
        # instead it encrypts the string with addiction of severtime and nonce
        # you can get this informationg by examming its codes in ssologin.js
        pw_string = '\t'.join([str(data['servertime']), str(data['nonce'])]) + '\n' + self.password
        # the key we go in prelogin data was in hexadecimal
        # we have to translate it to decimal first
        key = rsa.PublicKey(int(data['pubkey'], 16), rsa_e)
        # do the encryption
        pw_encypted = rsa.encrypt(pw_string.encode('utf-8'), key)
        self.password = ''  # clean the password for safty concern
        # again anything needed to be transmitted trought network
        # it has to be in hexadecimal encode
        password = binascii.b2a_hex(pw_encypted)
        return password

    def build_post_data(self, data):
        post_data = {
            'entry': 'weibo',
            'gateway': '1',
            'from': '',
            'savestate': '7',
            'qrcode_flag': 'false',
            'useticket': '1',
            'pagerefer': 'https://passport.weibo.com/visitor/visitor?entry=miniblog&a=enter&url=https%3A%2F%2Fweibo.com%2F&domain=.weibo.com&ua=php-sso_sdk_client-0.6.28',
            'vsnf': '1',
            'su': self.get_encrypted_name(),
            'service': 'miniblog',
            'servertime': data['servertime'],
            'nonce': data['nonce'],
            'pwencode': 'rsa2',
            'rsakv': data['rsakv'],
            'sp': self.get_encrypted_password(data),
            'sr': '1920*1080',
            'encoding': 'UTF-8',
            'prelt': '31',
            'url': 'https://weibo.com/ajaxlogin.php?framelogin=1&callback=parent.sinaSSOController.feedBackUrlCallBack',
            'returntype': 'META'}
        return post_data

    def publish_text(self, text=None):
        """ publish text (str) to weibo
        """
        # continue from the login session
        session = self.session
        # build time_slug which will be used in building the post url
        unix_time = int(time.mktime(datetime.datetime.utcnow().timetuple()))
        ms = randint(0, 999)
        time_slug = str(unix_time) + '{0:03d}'.format(ms)
        # build the post url
        url = 'https://weibo.com/aj/mblog/add?ajwvr=6&__rnd={}'.format(time_slug)
        # a helper methond for building the payload properly
        def build_publish_payload(input_text):
            """ return encoding payload (str)
                the input_text (str) will be encode like a Url
            """
            text_urllike = urllib.request.quote(input_text)
            payload = 'location=v6_content_home&text={}&appkey=&style_type=1&pic_id=&tid=&pdetail=&mid=&isReEdit=false&rank=0&rankid=&module=stissue&pub_source=main_&pub_type=dialog&isPri=0&_t=0'.format(text_urllike)
            return payload
        # build the payload
        payload = build_publish_payload(text)
        headers = {
            'Accept': '*/*',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7',
            'Connection': 'keep-alive',
            'Content-Length': str(len(payload)),
            'Content-Type': 'application/x-www-form-urlencoded',
            # 'Cookie':
            'DNT': '1',
            'Host': 'weibo.com',
            'Origin': 'https://weibo.com',
            'Referer': self.home_url,
            'User-Agent': choice(DESKTOP_USER_AGENTS),
            'X-Requested-With': 'XMLHttpRequest'}

        session.post(url, headers=headers, data=payload)


def pick_choice_lines(path_to_file, path_to_log_file, file_encode='UTF-8'):
    """ pick the lines (list) that was not published before
    """
    file_lines = []
    with open(path_to_file) as opf:
        for line in opf.readlines():
            line_text = line.strip()
            if line_text:
                file_lines.append(line_text)
    log_file_lines = []
    with open(path_to_log_file) as oplf:
        for line in oplf.readlines():
            line_text = line.strip()
            if line_text:
                log_file_lines.append(line_text)
    line_basket = []
    for line in file_lines:
        if line not in log_file_lines:
            line_basket.append(line)
    return line_basket


def pick_lines(lines, limit_words=2000):
    """ pick the lines (str) with the exact order but no more than limit_words (int)
    """
    word_count = 0
    line_basket = []
    for line in lines:
        line_text = line.strip()
        if line_text:
            line_basket.append(line_text)
            word_count += len(line_text)
            if word_count > limit_words:
                line_basket.pop()
                break
    return line_basket


def get_lines(path_to_book, file_encode='UTF-8', limit_words=2000):
    """ path_to_book (str) e.g. 'books/红楼梦'
        under this path, there should be chapters of the books
        return the chapter name (str) and lines (list) with word limit
    """
    # parse the path to get the book name
    path_pattern = re.compile(r'(.*)\/(.*)\/*')
    category, book_name = (path_pattern.search(path_to_book).group(i) for i in [1, 2])
    # book_files is a list of chapter names
    book_files = []
    temp_book_files = sorted(glob.glob(os.path.join(path_to_book, '*')))
    for path in temp_book_files:
        book_files.append(os.path.basename(path))
    # build the path to book chapter file and corresponse log file
    path_to_book_file = os.path.join(path_to_book, book_files[0])
    path_to_log_file = 'publish_log/' + book_name + '/' + book_files[0]
    # ensure that the log file exists
    if not os.path.exists('publish_log/' + book_name):
        os.mkdir('publish_log/' + book_name)
    if not os.path.exists(path_to_log_file):
        f = open(path_to_log_file, mode='w+')
        f.close()
    choice_lines = pick_choice_lines(path_to_book_file, path_to_log_file)
    if choice_lines:
        # pick the to publish lines
        lines = pick_lines(choice_lines, limit_words=limit_words)
        return path_to_log_file, book_files[0], lines
    else:
        # choice_lines is empty
        # which means all the lines in that file are all published before
        # then delete the log file and corresponse book chapter file
        # and moving on working on the next book chapter file
        os.remove(path_to_book_file)
        os.remove(path_to_log_file)
        return get_lines(path_to_book)


def get_launch_times(low_time, up_time, choice_count_num):
    """ return a list of distinct times (time.time()) (unix time)
        between low_time and up_time
        with exactly choice_count_num (int) of time
    """
    time_basket = []
    while len(set(time_basket)) < choice_count_num:
        time_stamp = randint(low_time+1, up_time-1)
        time_basket.append(time_stamp)
    return sorted(list(set(time_basket)))


def make_schedual(lines_len):
    """ A for 7:00~11:00
        B for 11:00~13:00
        C for 13:00~18:00
        D for 18:00~20:00
        E for 20:00~00:00
        lines_len (int) length of the lines
        retun a list of unix time of the day to represent schedual
    """
    TIME7 = 7*60*60
    TIME11 = 11*60*60
    TIME13 = 13*60*60
    TIME18 = 18*60*60
    TIME20 = 20*60*60
    TIME24 = 24*60*60
    epoch_choice = ['A', 'B', 'B', 'C', 'D', 'D', 'E', 'E']
    epoch_time = {
            'A': (TIME7, TIME11),
            'B': (TIME11, TIME13),
            'C': (TIME13, TIME18),
            'D': (TIME18, TIME20),
            'E': (TIME20, TIME24)}
    epoch_count = {'A': 0, 'B': 0, 'C': 0, 'D': 0, 'E': 0}
    for _ in range(lines_len):
        epoch = choice(epoch_choice)
        epoch_count[epoch] += 1

    schedual = []
    for epoch, count in epoch_count.items():
        lt, ut = epoch_time[epoch]
        schedual.extend(get_launch_times(lt, ut, count))
    return sorted(schedual)


def main():
    path_to_log_file, chapter, lines = get_lines('books/红楼梦')
    schedual = make_schedual(len(lines))
    # reverse so as to pop the first one out first
    lines.reverse()
    schedual.reverse()
    while schedual:
        launch_time = schedual.pop()
        line = lines.pop()
        p_text = line + '\n\n#红楼梦# ' + chapter[5:-4] + '\n\n#碎片化读经典#'
        day_cycle = 24*60*60  # in seconds
        # adjust to current time zone
        current_time = int(time.time() + 8*60*60) % day_cycle
        if current_time > launch_time:
            connect = weibo_desktop('your_weibo_username', 'your_passwd')
            connect.publish_text(text=p_text)
            # write the lines to log file
            with open(path_to_log_file, mode='a', encoding='UTF-8') as f:
                    write_line = line + '\n'
                    f.write(write_line)
            time.sleep(randint(10, 30))
        else:
            time.sleep(launch_time - current_time)
            connect = weibo_desktop('weibo_username', 'weibo_passwd')
            connect.publish_text(text=p_text)
            # write the lines to log file
            with open(path_to_log_file, mode='a', encoding='UTF-8') as f:
                    write_line = line + '\n'
                    f.write(write_line)


if __name__ == '__main__':
    main()
